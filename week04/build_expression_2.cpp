#include <queue>
#include <deque>
#include <iostream>
#include <string>
#include <sstream>

using namespace std;

int main(){
   string command_txt = "8\n"
                        "3\n"
                        "+ 3\n"
                        "/ 6\n"
                        "+ 1\n";

   stringstream s(command_txt);

   string command;
   int number_operations;
   int first_number;
   int number;
   char operation;
   
   cin >> first_number;
   cin >> number_operations;
   //s >> first_number;
   //s >> number_operations;

   deque<string> output;

   output.push_back(to_string(first_number));

    char previous_operation;
    bool higher = false;
    for (int i = 0; i < number_operations; i++){
        //s >> operation;
        //s >> number;
        cin >> operation;
        cin >> number;
        if (((operation == '*') & (previous_operation == '-')) |
         ((operation == '*') & (previous_operation == '+')) | 
         ((operation == '/') & (previous_operation == '-')) | 
         ((operation == '/') & (previous_operation == '+'))){
             higher = true;
         }
         else{
             higher = false;
         }
        if (higher){
          output.push_front("(");
        }
        if (higher){
          output.push_back(")");
        }
        output.push_back(" ");
        output.push_back(string(1, operation));
        output.push_back(" ");
        output.push_back(to_string(number));
        previous_operation = operation;
    }


    
    for (auto it=output.begin(); it != output.end(); ++it){
        cout << *it;
    }
    cout << endl;
    return 0;
}