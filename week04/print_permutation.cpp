#include <algorithm>
#include <iostream>
#include <vector>

using namespace std;


void PrintVec(const vector<int>& vec){
    for (const auto v: vec){
        cout << v << " ";
    }
    cout << endl;
}

int main(){
    int n;
    cin >> n;
    vector<int> vec;
    for (int i=0; i<n; i++){
        vec.push_back(i+1);
    }
    reverse(begin(vec), end(vec));
    PrintVec(vec);
    while (next_permutation(begin(vec), end(vec),
     [](int lhs, int rhs){return lhs > rhs;})){
        PrintVec(vec);
    }
    return 0;
}