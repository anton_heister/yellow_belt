#include <set>
#include <algorithm>
#include <vector>
#include <iostream>
#include <utility>

using namespace std;


void PrintSet(const set<int>& numbers){
    for (auto x: numbers){
        cout << x << " ";
    }
    cout << endl;
}

void PrintSet(const set<int, greater<int>>& numbers){
    for (auto x: numbers){
        cout << x << " ";
    }
    cout << endl;
}


set<int>::const_iterator FindNearestElement(
    const set<int>& numbers,
    int border){
        auto first_not_less = numbers.lower_bound(border);
        if (first_not_less == numbers.begin()){
            return first_not_less;
        }
        else{
            auto last_less = prev(first_not_less);
            if (first_not_less == numbers.end()){
                return last_less;
            }
            else{
              bool take_not_less = (border - *last_less) > (*first_not_less- border);
              return take_not_less ? first_not_less : last_less;
            }
        }
    }



int main() {
  set<int> numbers = {1, 4, 6};
  cout <<
      *FindNearestElement(numbers, 0) << " " << endl <<  
      *FindNearestElement(numbers, 3) << " " << endl << 
      *FindNearestElement(numbers, 5) << " " << endl << 
      *FindNearestElement(numbers, 6) << " " << endl << 
      *FindNearestElement(numbers, 100) << endl;
      
  set<int> empty_set;
  
  cout << (FindNearestElement(empty_set, 8) == end(empty_set)) << endl;
  return 0;
}
