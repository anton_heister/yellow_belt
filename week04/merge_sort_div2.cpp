
#include <iostream>
#include <vector>

using namespace std;


template <typename T>
void PrintVec(const vector<T>& vec){
    for (const T v: vec){
        cout << v << " ";
    }
    cout << endl;
}

template <typename RandomIt>
void PrintVec(RandomIt it_start, RandomIt it_stop){
    while (it_start != it_stop){
        cout << *(it_start++) << " ";
    }
    cout << endl;
}


template <typename RandomIt>
void MergeSort(RandomIt range_begin, RandomIt range_end){
    if ((range_end - range_begin) < 2){
        return;
    }
    vector<typename RandomIt::value_type> elements(range_begin, range_end);
    size_t const half_size = elements.size() / 2;
    MergeSort(begin(elements), begin(elements) + half_size);
    MergeSort(begin(elements) + half_size, end(elements));
    merge(
        begin(elements), begin(elements) + half_size,
        begin(elements) + half_size, end(elements),
        range_begin);
}


int main() {
  vector<int> v = {6, 4, 7, 6, 4, 4, 0, 1};
  PrintVec(v);
  MergeSort(begin(v), end(v));
  for (int x : v) {
    cout << x << " ";
  }
  cout << endl;
  return 0;
}