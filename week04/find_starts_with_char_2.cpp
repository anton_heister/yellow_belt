#include <iostream>
#include <vector>
#include <algorithm>
#include <utility>

using namespace std;


string incrementPrefix_(const string& prefix){
    string s(prefix);
    if (*prev(s.end()) != 'z'){
        *prev(s.end()) = ++*prev(s.end());
        return s;
    }
    else if (count(s.begin(), s.end(), 'z') == s.size()){
        cout << "all z" << endl;
        string s_(s.size() + 1, 'a');
        return s_;
    }
    else{
      auto it = rbegin(s);
      while((*it == 'z') & (it != rend(s))){
          *it = 'a';
          ++it;
      }
    }
    return s;
}

string incrementPrefix(const string& prefix){
    string s(prefix);
    *prev(s.end()) = ++*prev(s.end());
    //if (*prev(s.end()) == 'z'){
    //}
    //else{

    //}
    return s;
}







template <typename RandomIt>
pair<RandomIt, RandomIt> FindStartsWith(
    RandomIt range_begin, RandomIt range_end,
    const string& prefix){
    string s(prefix);
    	if (prefix.empty() || range_begin == range_end)
	{
		return {range_begin, range_begin};
	}
    auto it_start = lower_bound(range_begin, range_end, s);
    s = incrementPrefix(s);
    auto it_stop = lower_bound(range_begin, range_end, s);
    if (it_start != range_end){
        return make_pair(it_start, it_stop);
    }
    else{ // no elements found
        return make_pair(it_stop, it_stop);
    }
}


int main() {

  string prefix("ab");
  string prefix1 = incrementPrefix(prefix);
  cout << prefix << endl;
  cout << prefix1 << endl;



  const vector<string> sorted_strings = {"moscow", "motovilikha", "murmansk"};
  
  const auto mo_result =
      FindStartsWith(begin(sorted_strings), end(sorted_strings), "mo");
  for (auto it = mo_result.first; it != mo_result.second; ++it) {
    cout << *it << " ";
  }
  cout << endl;
  
  const auto mt_result =
      FindStartsWith(begin(sorted_strings), end(sorted_strings), "mt");
  cout << (mt_result.first - begin(sorted_strings)) << " " <<
      (mt_result.second - begin(sorted_strings)) << endl;
  
  const auto na_result =
      FindStartsWith(begin(sorted_strings), end(sorted_strings), "na");
  cout << (na_result.first - begin(sorted_strings)) << " " <<
      (na_result.second - begin(sorted_strings)) << endl;


  return 0;
}
