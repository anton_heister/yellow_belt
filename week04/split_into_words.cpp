#include <iostream>
#include <string>
#include <vector>
#include <algorithm>


using namespace std;

vector<string> SplitIntoWords(const string& s){
    vector<string> result;
    auto begin_it = begin(s);
    while(begin_it < end(s)){
      auto end_it = find(begin_it, end(s), ' ');
      string word{begin_it, end_it};
      result.push_back(word);
      begin_it = end_it + 1;
    }
    return result;
}


int main() {
  string s = "C Cpp Java Python";

  vector<string> words = SplitIntoWords(s);
  cout << words.size() << " ";
  for (auto it = begin(words); it != end(words); ++it) {
    if (it != begin(words)) {
      cout << "/";
    }
    cout << *it;
  }
  cout << endl;
  
  return 0;
}