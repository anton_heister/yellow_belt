#include <queue>
#include <deque>
#include <iostream>
#include <string>
#include <sstream>

using namespace std;

int main(){
   string command_txt = "8\n"
                        "3\n"
                        "* 3\n"
                        "- 6\n"
                        "/ 1\n";

   stringstream s(command_txt);

   string command;
   int number_operations;
   int first_number;
   int number;
   char operation;


   
   cin >> first_number;
   cin >> number_operations;
   
   //s >> first_number;
   //s >> number_operations;

   deque<string> output;

   if (number_operations > 0){
      output.push_front("(");
   }
   output.push_back(to_string(first_number));
   if (number_operations > 0){
      output.push_back(")");
   }

    for (int i = 0; i < number_operations; i++){
        cin >> operation;
        cin >> number;
        //if (operation == '-' && number < 0){
        //    operation = '+';
        //    number *= -1;
        //}
        if (i != number_operations-1){
          output.push_front("(");
        }
        output.push_back(" ");
        output.push_back(string(1, operation));
        output.push_back(" ");

        //if (number < 0){
        //  output.push_back("(");
        //}
        output.push_back(to_string(number));
        //if (number < 0){
        //  output.push_back(")");
        //}

        if (i != number_operations - 1){
          output.push_back(")");
          }
    }


    
    for (auto it=output.begin(); it != output.end(); ++it){
        cout << *it;
    }
    cout << endl;
    return 0;
}