#include <iostream>
#include <vector>
#include <algorithm>
#include <utility>

using namespace std;


template <typename RandomIt>
pair<RandomIt, RandomIt> FindStartsWith_(
    RandomIt range_begin, RandomIt range_end,
    char prefix){

    auto it_start = find_if(range_begin, range_end,
    [prefix](const typename RandomIt::value_type& value){return value[0] == prefix;});
    auto it_stop = find_if(range_begin, range_end,
    [prefix](const typename RandomIt::value_type& value){return value[0] > prefix;});
    if (it_start != range_end){
        return make_pair(it_start, it_stop);
    }
    else{ // no elements found
        return make_pair(it_stop, it_stop);
    }
}


template <typename RandomIt>
pair<RandomIt, RandomIt> FindStartsWith(
    RandomIt range_begin, RandomIt range_end,
    char prefix){
    string s(1, static_cast<char>(prefix));
    auto it_start = lower_bound(range_begin, range_end, s);
    s = (prefix + 1);
    auto it_stop = upper_bound(range_begin, range_end, s);

    if (it_start != range_end){
        return make_pair(it_start, it_stop);
    }
    else{ // no elements found
        return make_pair(it_stop, it_stop);
    }
}



int main() {
  //const vector<string> sorted_strings = {"ancara", "beirut", "moscow", "minsk", "murmansk", "new york", "vologda"};
  const vector<string> sorted_strings = {"loh", "zoo"};
  //const vector<string> sorted_strings = {"moscow", "murmansk", "vologda"};
  const auto m_result =
      FindStartsWith(begin(sorted_strings), end(sorted_strings), 'm');
  for (auto it = m_result.first; it != m_result.second; ++it) {
    cout << *it << " ";
  }
  cout << endl;
  
  const auto p_result =
      FindStartsWith(begin(sorted_strings), end(sorted_strings), 'p');
  cout << (p_result.first - begin(sorted_strings)) << " " <<
      (p_result.second - begin(sorted_strings)) << endl;
  
  const auto z_result =
      FindStartsWith(begin(sorted_strings), end(sorted_strings), 'z');
  cout << (z_result.first - begin(sorted_strings)) << " " <<
      (z_result.second - begin(sorted_strings)) << endl;
  return 0;
}