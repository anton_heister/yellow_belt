#include <iostream>
#include <map>
#include <set>
#include <sstream>
#include <stdexcept>
#include <string>
#include <vector>

using namespace std;

template <class T>
ostream& operator << (ostream& os, const vector<T>& s) {
  os << "{";
  bool first = true;
  for (const auto& x : s) {
    if (!first) {
      os << ", ";
    }
    first = false;
    os << x;
  }
  return os << "}";
}

template <class T>
ostream& operator << (ostream& os, const set<T>& s) {
  os << "{";
  bool first = true;
  for (const auto& x : s) {
    if (!first) {
      os << ", ";
    }
    first = false;
    os << x;
  }
  return os << "}";
}

template <class K, class V>
ostream& operator << (ostream& os, const map<K, V>& m) {
  os << "{";
  bool first = true;
  for (const auto& kv : m) {
    if (!first) {
      os << ", ";
    }
    first = false;
    os << kv.first << ": " << kv.second;
  }
  return os << "}";
}

template<class T, class U>
void AssertEqual(const T& t, const U& u, const string& hint = {}) {
  if (t != u) {
    ostringstream os;
    os << "Assertion failed: " << t << " != " << u;
    if (!hint.empty()) {
       os << " hint: " << hint;
    }
    throw runtime_error(os.str());
  }
}

void Assert(bool b, const string& hint) {
  AssertEqual(b, true, hint);
}

class TestRunner {
public:
  template <class TestFunc>
  void RunTest(TestFunc func, const string& test_name) {
    try {
      func();
      cerr << test_name << " OK" << endl;
    } catch (exception& e) {
      ++fail_count;
      cerr << test_name << " fail: " << e.what() << endl;
    } catch (...) {
      ++fail_count;
      cerr << "Unknown exception caught" << endl;
    }
  }

  ~TestRunner() {
    if (fail_count > 0) {
      cerr << fail_count << " unit tests failed. Terminate" << endl;
      exit(1);
    }
  }

private:
  int fail_count = 0;
};

/*
class Rational {
public:
  Rational();
  Rational(int numerator, int denominator) {
  }

  int Numerator() const {
  }

  int Denominator() const {
  }
};
*/


bool compare_rationals(const Rational& lhs, const Rational& rhs){
    return (lhs.Numerator() == rhs.Numerator()) &
     (lhs.Denominator() == rhs.Denominator());
}


void TestDefaultConstructor(){
    Rational p;
    Assert(compare_rationals(Rational(0, 1), p), "Default constructor");
}

void TestReduce(){
{
    Rational p(3, 6);
    Assert(compare_rationals(Rational(1, 2), p), "Positive reduce");
}
{
    Rational p(-3, 6);
    Assert(compare_rationals(Rational(-1, 2), p), "Negative reduce");
}
}

void TestMinus(){
{
    Rational p(-1, -2);
    Assert(compare_rationals(Rational(1, 2), p), "Two negatives");

}
//    auto ref = make_pair(1, 2);
//    auto got = make_pair(p.Numerator(), p.Denominator());
//    AssertEqual(ref, got, "Two negatives");
{
    Rational p(1, -2);
    Assert(compare_rationals(Rational(-1, 2), p), "Denominator negative");
}
//    auto ref = make_pair(-1, 2);
//    auto got = make_pair(p.Numerator(), p.Denominator());
//    AssertEqual(ref, got, "Denominator negative");
{
    Rational p(-1, 2);
    Assert(compare_rationals(Rational(-1, 2), p), "Numerator negative");
}
//    auto ref = make_pair(-1, 2);
//    auto got = make_pair(p.Numerator(), p.Denominator());
//    AssertEqual(ref, got, "Numerator negative");
}

void TestZeroNumerator(){
    Rational p(0, -2);
    Assert(compare_rationals(Rational(0, 1), p), "Zero num, negative den");
//    auto ref = make_pair(0, 1);
//    auto got = make_pair(p.Numerator(), p.Denominator());
//    AssertEqual(ref, got, "Zero num, negative den");
}



int main() {
  TestRunner runner;
  runner.RunTest(TestDefaultConstructor, "TestDefaultConstructor");
  runner.RunTest(TestReduce, "TestReduce");
  runner.RunTest(TestMinus, "TestMinus");
  runner.RunTest(TestZeroNumerator, "TestZeroNumerator");
  return 0;
}