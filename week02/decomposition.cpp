#include <string>
#include <iostream>
#include <cassert>
#include <vector>
#include <map>
#include <sstream>

using namespace std;

enum class QueryType {
  NewBus,
  BusesForStop,
  StopsForBus,
  AllBuses
};

struct Query {
  QueryType type;
  string bus;
  string stop;
  vector<string> stops;
};

istream& operator >> (istream& is, Query& q) {
    q.bus = "";
    q.stop = "";
    q.stops.clear();

    string cmd;
    is >> cmd;
    if (cmd == "ALL_BUSES"){
        q.type = QueryType::AllBuses;
    }
    else if (cmd == "BUSES_FOR_STOP"){
        q.type = QueryType::BusesForStop;
        is >> q.stop;
    }
    else if (cmd == "STOPS_FOR_BUS"){
      q.type = QueryType::StopsForBus;
      is >> q.bus;
    }
    else if (cmd == "NEW_BUS"){
        q.type = QueryType::NewBus;
        is >> q.bus;
        int nstops;
        is >> nstops;
        string stop;
        for (int i=0;i<nstops; i++){
            is >> stop;
            q.stops.push_back(stop);
        }
    }
    return is;
}

struct BusesForStopResponse {
    string stop;
    vector<string> buses;
};

ostream& operator << (ostream& os, const BusesForStopResponse& r) {
    if (r.buses.size() > 0){
        for (const auto& bus: r.buses){
            os << bus << " ";
        }
    }
    else{
        os << "No stop";
    }
  return os;
}

struct StopsForBusResponse {
    string bus = "";
    // map<stop, buses>
    map<string, vector<string>> stops_to_buses;
    map<string, vector<string>> buses_to_stops;
};

ostream& operator << (ostream& os, const StopsForBusResponse& r) {
    //if (r.stops_to_buses.size() == 0){
    //    cout << r.bus << " " << endl;
    //if (r.stops_to_buses.count(r.bus) == 0){
    if (r.buses_to_stops.count(r.bus) == 0){
        os << "No bus";
    }
    else{
        size_t nstops = r.buses_to_stops.at(r.bus).size();
        size_t counter = 0;
        for (const auto& stop: r.buses_to_stops.at(r.bus)){
            os << "Stop " + stop + ": ";
            if (r.stops_to_buses.at(stop).size() == 1){
                os << "no interchange";
            }
            else{
                for (const auto& bus: r.stops_to_buses.at(stop)){
                    if (bus != r.bus){
                        os << bus << " ";
                    }
                }
            }
            if (counter < nstops - 1){
                counter++;
                os << endl;
            }
        }

    }
  return os;
}

struct AllBusesResponse {
    map<string, vector<string>> buses;
};

ostream& operator << (ostream& os, const AllBusesResponse& r) {
    size_t nbuses = r.buses.size();
    if (nbuses == 0){
        os << "No buses";
        return os;
    }
    else{
        size_t counter = 0;
        for (const auto& [bus, stops]: r.buses){
            os << "Bus " + bus + ": ";
            for (const auto& stop: stops){
                os << stop << " ";
            }
            counter++;
            if (counter < nbuses){
                os << endl;
            }
        }
    return os;
    }
}

class BusManager {
private:
  map<string, vector<string>> buses_to_stops;
  map<string, vector<string>> stops_to_buses;
public:
  void AddBus(const string& bus, const vector<string>& stops) {
      for (const string& stop : stops) {
        stops_to_buses[stop].push_back(bus);
        buses_to_stops[bus].push_back(stop);
      }
  }

  BusesForStopResponse GetBusesForStop(const string& stop) const {
      BusesForStopResponse response;
      response.stop = stop;
      if (stops_to_buses.count(stop) != 0){
        for (const auto& bus: stops_to_buses.at(stop)){
            response.buses.push_back(bus);
        }
      }
      return response;
  }

  StopsForBusResponse GetStopsForBus(const string& bus) const {
    StopsForBusResponse response;
    response.bus = bus;
    response.stops_to_buses = stops_to_buses;
    response.buses_to_stops = buses_to_stops;

//    if (stops_to_buses.count(bus) != 0){
//      for (const auto& stop: stops_to_buses.at(bus)){
//          response.stops_to_buses[bus].push_back(stop);
//      }
//      }
    return response;
  }

  AllBusesResponse GetAllBuses() const {
    AllBusesResponse response;
    for (const auto& [bus, stops]: buses_to_stops) {
      for (const string& stop : stops) {
          response.buses[bus].push_back(stop);
      }
    }
  return response;
  }
};


int main() {
  string cmds = "10\n"
                "ALL_BUSES\n"
                "BUSES_FOR_STOP Marushkino\n"
                "STOPS_FOR_BUS 32K\n"
                "NEW_BUS 32 3 Tolstopaltsevo Marushkino Vnukovo\n"
                "NEW_BUS 32K 6 Tolstopaltsevo Marushkino Vnukovo Peredelkino Solntsevo Skolkovo\n"
                "BUSES_FOR_STOP Vnukovo\n"
                "NEW_BUS 950 6 Kokoshkino Marushkino Vnukovo Peredelkino Solntsevo Troparyovo\n"
                "NEW_BUS 272 4 Vnukovo Moskovsky Rumyantsevo Troparyovo\n"
                "STOPS_FOR_BUS 272\n"
                "ALL_BUSES\n";
  //string cmds = "2\n"
  //              "NEW_BUS 32 3 Tolstopaltsevo Marushkino Vnukovo\n"
  //              "STOPS_FOR_BUS 32K\n";
  stringstream cin_string(cmds);
  int query_count;
  Query q;
  //cin >> query_count;
  cin_string >> query_count;
  BusManager bm;
  for (int i = 0; i < query_count; ++i) {
    //cin >> q;
    cin_string >> q;
    switch (q.type) {
    case QueryType::NewBus:
      bm.AddBus(q.bus, q.stops);
      break;
    case QueryType::BusesForStop:
      cout << bm.GetBusesForStop(q.stop) << endl;
      break;
    case QueryType::StopsForBus:
      cout << bm.GetStopsForBus(q.bus) << endl;
      break;
    case QueryType::AllBuses:
      cout << bm.GetAllBuses() << endl;
      break;
    }
  }
  return 0;
}