#include <iostream>
#include <map>
#include <set>
#include <sstream>
#include <stdexcept>
#include <string>
#include <vector>

using namespace std;

template <class T>
ostream& operator << (ostream& os, const vector<T>& s) {
  os << "{";
  bool first = true;
  for (const auto& x : s) {
    if (!first) {
      os << ", ";
    }
    first = false;
    os << x;
  }
  return os << "}";
}

template <class T>
ostream& operator << (ostream& os, const set<T>& s) {
  os << "{";
  bool first = true;
  for (const auto& x : s) {
    if (!first) {
      os << ", ";
    }
    first = false;
    os << x;
  }
  return os << "}";
}

template <class K, class V>
ostream& operator << (ostream& os, const map<K, V>& m) {
  os << "{";
  bool first = true;
  for (const auto& kv : m) {
    if (!first) {
      os << ", ";
    }
    first = false;
    os << kv.first << ": " << kv.second;
  }
  return os << "}";
}

template<class T, class U>
void AssertEqual(const T& t, const U& u, const string& hint = {}) {
  if (t != u) {
    ostringstream os;
    os << "Assertion failed: " << t << " != " << u;
    if (!hint.empty()) {
      os << " hint: " << hint;
    }
    throw runtime_error(os.str());
  }
}

void Assert(bool b, const string& hint) {
  AssertEqual(b, true, hint);
}

class TestRunner {
public:
  template <class TestFunc>
  void RunTest(TestFunc func, const string& test_name) {
    try {
      func();
      cerr << test_name << " OK" << endl;
    } catch (exception& e) {
      ++fail_count;
      cerr << test_name << " fail: " << e.what() << endl;
    } catch (...) {
      ++fail_count;
      cerr << "Unknown exception caught" << endl;
    }
  }

  ~TestRunner() {
    if (fail_count > 0) {
      cerr << fail_count << " unit tests failed. Terminate" << endl;
      exit(1);
    }
  }

private:
  int fail_count = 0;
};

/*
class Person {
public:
  void ChangeFirstName(int year, const string& first_name) {
  }
  void ChangeLastName(int year, const string& last_name) {
  }
  string GetFullName(int year) {
  }
};
*/

void TestPerson(){
    Person p;
    p.ChangeFirstName(1989, "Ivan");
    string output = p.GetFullName(1980);
    AssertEqual("Incognito", output, "Name/suranme unknown");
    output = p.GetFullName(1989);
    AssertEqual("Ivan with unknown last name", output, "Unknown last name");
    p.ChangeLastName(1989, "Stolin");
    output = p.GetFullName(1989);
    AssertEqual("Ivan Stolin", output, "First+last name");
    Person b;
    b.ChangeLastName(1888, "Lomakin");
    output = b.GetFullName(1888);
    AssertEqual("Lomakin with unknown first name", output,
     "Unknown first name");
}


void TestGetFullName(){
    /*
    Если к данному году не случилось ни одного изменения фамилии и имени,
     верните строку "Incognito".
    Если к данному году случилось изменение фамилии, но не было ни одного
     изменения имени, верните "last_name with unknown first name".
    Если к данному году случилось изменение имени, но не было ни одного
     изменения фамилии, верните "first_name with unknown last name".
    */

}

int main() {
  TestRunner runner;
  runner.RunTest(TestPerson, "TestPerson");
  return 0;
}