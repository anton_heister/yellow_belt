#include <iostream>
#include <vector>
#include <algorithm>

int main(){
    int n;
    std::cin >> n;
    int64_t tmp;
    std::vector<int64_t> temperatures;
    int64_t mean_temperature = 0;
    for (int i=0; i < n; i++){
        std::cin >> tmp;
        temperatures.push_back(tmp);
        mean_temperature += tmp;
    }
    mean_temperature /= static_cast<int>(temperatures.size());
    std::vector<int64_t> result;
    for (int i = 0; i < n; i++){
        if (temperatures[i] > mean_temperature){
            result.push_back(i);
        }
    }
    std::cout << result.size() << std::endl;
    for (const auto& t: result){
        std::cout << t << " ";
    }
    return 0;
}