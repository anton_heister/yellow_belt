#include <iostream>
#include <vector>
#include <fstream>
#include <stdexcept>
#include <sstream>

using namespace std;

class Matrix{
    public:
    Matrix(){
        rows = 0;
        columns = 0;
        InitializeStorage();
    };
    Matrix(int n_row, int n_col){
        if ((n_row < 0) || (n_col < 0)){
            throw out_of_range("");
        }
        rows = n_row;
        columns = n_col;
        InitializeStorage();
    };
    void Reset (int n_row, int n_col){
        if ((n_row < 0) || (n_col < 0)){
            throw out_of_range("");
        }
        rows = n_row;
        columns = n_col;
        InitializeStorage();
        for (int row=0; row < n_row; row++){
            for (int col=0; col < n_col; col++){
                storage[row][col] = 0;
            }
        }
    };
    bool IsEmpty() const {
        return ((rows == 0) || (columns == 0));
    };
    int At (int n_row, int n_col) const {
        if ((n_row < 0) || (n_col < 0)){
            throw out_of_range("");
        }
        return storage.at(n_row).at(n_col);
    };
    int& At (int n_row, int n_col){
        if ((n_row < 0) || (n_col < 0)){
            throw out_of_range("");
        }
        return storage.at(n_row).at(n_col);
    };
    int GetNumRows() const {
        return rows; 
    };
    int GetNumColumns() const {
        return columns; 
    };
    private:
    int rows, columns;
    vector<vector<int>> storage;

    void InitializeStorage(){
      storage.resize(rows);
      for ( int i = 0 ; i < rows ; i++ ){
         storage[i].resize(columns);
    };
    };
};

istream& operator >> (istream& input, Matrix& matrix){
    int n_row, n_col, tmp;
    input >> n_row >> n_col;
    matrix.Reset(n_row, n_col);
    for (int row=0; row < n_row; row++){
        for (int col=0; col < n_col; col++){
            input >> tmp;
            matrix.At(row, col) = tmp;
        }
   }
    return input;
}

ostream& operator << (ostream& output, const Matrix& matrix){
    int n_row, n_col;
    n_row = matrix.GetNumRows();
    n_col = matrix.GetNumColumns();
    output << n_row << " " << n_col << endl;
    for (int row=0; row < n_row; row++){
        for (int col=0; col < n_col; col++){
            auto tmp = matrix.At(row, col);
            output << tmp << " ";
        }
        output << endl;
    }
    return output;
}


Matrix operator + (const Matrix& lhs, const Matrix& rhs){
    int lhs_nrow, lhs_ncol, rhs_nrow, rhs_ncol;
    lhs_nrow = lhs.GetNumRows();
    lhs_ncol = lhs.GetNumColumns();
    rhs_nrow = rhs.GetNumRows();
    rhs_ncol = rhs.GetNumColumns();
    if (lhs.IsEmpty() && rhs.IsEmpty()){
        return Matrix();
    }
    else if ((lhs_nrow != rhs_nrow) || (lhs_ncol != rhs_ncol)){
        string error_message = "Incorrect dimensions (" + to_string(lhs_nrow) + "," +
         to_string(lhs_ncol) + ") != (" + to_string(rhs_ncol) + "," +
         to_string(rhs_nrow) + ")";
        throw invalid_argument(error_message);

    }
    Matrix sum_matrix(lhs_nrow, lhs_ncol);
    for (int row=0; row < lhs_nrow; row++){
        for (int col=0; col < lhs_ncol; col++){
            sum_matrix.At(row, col) = lhs.At(row, col) + rhs.At(row, col);
        }
    }
    return sum_matrix;
}

bool operator == (const Matrix& lhs, const Matrix& rhs){
    if (lhs.IsEmpty() && rhs.IsEmpty()){
        return true;
    }
    if ( (lhs.GetNumRows() == rhs.GetNumRows() ) &&
        ( lhs.GetNumColumns() == rhs.GetNumColumns() ) ){
            int nrow, ncol;
            nrow = lhs.GetNumRows();
            ncol = lhs.GetNumColumns();
            bool result = true;
            for (int row = 0; row < nrow; row++){
                for (int col=0; col < ncol; col++){
                    result = lhs.At(row, col) == rhs.At(row, col);
                    if (!result){
                        return false;
                    }
                }
            }
        return result;
        }
    else{
        return false;
    }
}

int main(){
  string s1 = "3 5\n"
  "6 4 -1 9 8\n"
  "12 1 2 9 -5\n"
  "-4 0 12 8 6\n";
  stringstream s1_stream(s1);
  string s2 = "3 5\n"
              "5 1 0 -8 23\n"
              "14 5 -6 6 9\n"
              "8 0 5 4 1\n";
  stringstream s2_stream(s2);

  Matrix one;
  Matrix two;
  s1_stream >> one;
  s2_stream >> two;
//cin >> one;
//  cout << "output:" << endl;
//  cout << one;

cout << "ONE" << endl;
cout << one << endl;
cout << "TWO" << endl;
cout << two << endl;

//  cout << one;
//  cin >> one >> two;
cout << "SUM" << endl;
  cout << one + two << endl;
int x = 2'000'000'000;
  return 0;
}