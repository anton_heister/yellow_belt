#include <iostream>
#include <vector>

using namespace std;

//unsigned int calculateMass(unsigned int w, unsigned int h, unsigned int d,
// unsigned int r){
//    return w * h * d * r;
//}

uint64_t calculateMass(uint64_t w, uint64_t h, uint64_t d,
 uint64_t r){
    return w * h * d * r;
}

int main(){
    //unsigned int n, r, w, h, d;
    uint64_t n, r, w, h, d;
    cin >> n >> r;
    uint64_t mass = 0;
    for (int i=0; i<n; i++){
        cin >> w >> h >> d;
        mass += calculateMass(w, h, d, r);
    }
    cout << mass << endl;
    return 0;
}