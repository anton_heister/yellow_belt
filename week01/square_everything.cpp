#include <iostream>
#include <map>
#include <vector>
#include <utility>

using namespace std; 

template <typename T>
T Sqr(const T& val);

template <typename T>
vector<T> Sqr(const vector<T>& vec);

template <typename Key, typename Value>
map<Key, Value> Sqr(const map<Key, Value>& input_map);

template <typename First, typename Second>
pair<First, Second> Sqr(const pair<First, Second>& input_pair);



// implementation
template <typename T>
T Sqr(const T& val){
    return val * val;
}

template <typename T>
vector<T> Sqr(const vector<T>& vec) {
    vector<T> output;
    for (auto& v: vec){
        output.push_back(Sqr(v));
    }
    return output;
}

template <typename First, typename Second>
pair<First, Second> Sqr(const pair<First, Second>& input_pair){
    pair<First, Second> output_pair;
    //output_pair = pair(input_pair.first * input_pair.first, 
    //                   input_pair.second * input_pair.second);
    output_pair = pair(Sqr(input_pair.first), 
                       Sqr(input_pair.second));
    return output_pair;
}

template <typename Key, typename Value>
map<Key, Value> Sqr(const map<Key, Value>& input_map){
    map<Key, Value> output_map;
    for (const auto& [key, value]: input_map){
        output_map[key] = Sqr(value);
    }
    return output_map;
}

int main(){
    // Пример вызова функции
    vector<int> v = {1, 2, 3};
    cout << "vector:";
    for (int x : Sqr(v)) {
      cout << ' ' << x;
    }
    cout << endl;
    
    //map<int, pair<int, int>> map_of_pairs = {
    //  {4, {2, 2}},
    //  {7, {4, 3}}
    //};


    map<int, pair<double, double>> map_of_pairs = {
      {4, {2.1, 2.3}},
      {7, {4.3, 3.3}}
    };

    cout << "map of pairs:" << endl;
    for (const auto& x : Sqr(map_of_pairs)) {
      cout << x.first << ' ' << x.second.first << ' ' << x.second.second << endl;
    }


    map<int, vector<double>> lol = {{1, {1.2, 2.2}}, {2, {12.2, 2.2}}};

    return 0;
}