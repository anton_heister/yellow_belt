//#include <iostream>
//#include <tuple>
//#include <map>
//#include <iomanip>
//#include <sstream>
//#include <vector>
#include <iostream>
#include <fstream>
#include <stdexcept>
#include <vector>
#include <set>
#include <map>
#include <string>
#include <sstream>

using namespace std;

/*
enum class TaskStatus {
  NEW,          // новая
  IN_PROGRESS,  // в разработке
  TESTING,      // на тестировании
  DONE          // завершена
};
using TasksInfo = map<TaskStatus, int>;
*/



void clean_task(TasksInfo& c){
        // erase all key-value pairs with zero values from c
    for(auto it = c.begin(); it != c.end(); )
        if(it->second == 0){
            it = c.erase(it);
        }
        else{
            ++it;
        }
};


TasksInfo sumUp(const TasksInfo& lhs, const TasksInfo& rhs){
    TasksInfo result;
    result[TaskStatus::NEW] = lhs.at(TaskStatus::NEW) + rhs.at(TaskStatus::NEW);
    result[TaskStatus::IN_PROGRESS] = lhs.at(TaskStatus::IN_PROGRESS) +
     rhs.at(TaskStatus::IN_PROGRESS);
    result[TaskStatus::TESTING] = lhs.at(TaskStatus::TESTING) +
     rhs.at(TaskStatus::TESTING);
    result[TaskStatus::DONE] = lhs.at(TaskStatus::DONE) +
     rhs.at(TaskStatus::DONE);
     return result;
}


class TeamTasks {
public:
  // Получить статистику по статусам задач конкретного разработчика
  const TasksInfo& GetPersonTasksInfo(const string& person) const{
      return storage.at(person);
  };
  
  // Добавить новую задачу (в статусе NEW) для конкретного разработчитка
  void AddNewTask(const string& person){
      storage[person][TaskStatus::NEW]++;
      clean_task(storage[person]);
  };

  // Обновить статусы по данному количеству задач конкретного разработчика,
  // подробности см. ниже
  
  tuple<TasksInfo, TasksInfo> PerformPersonTasks(
      const string& person, int task_count){
          if (storage.count(person) == 0){
              return {};
          }
          else{
              int num_tasks = 0;
              for (const auto& [key, value]: storage[person]){
                  num_tasks+=value;
              }
              if (num_tasks == 0){
                  return {};
              }
              if (task_count > num_tasks){
                  task_count = num_tasks;
              }
              TasksInfo updated_tasks;
              TasksInfo untouched_tasks = storage[person];
              for (int i=0; i < task_count; i++){
                  if (untouched_tasks[TaskStatus::NEW] > 0){
                      untouched_tasks[TaskStatus::NEW]--;
                      updated_tasks[TaskStatus::IN_PROGRESS]++;
                  }
                  else if (untouched_tasks[TaskStatus::IN_PROGRESS] > 0){
                      untouched_tasks[TaskStatus::IN_PROGRESS]--;
                      updated_tasks[TaskStatus::TESTING]++;
                  }
                  else if (untouched_tasks[TaskStatus::TESTING] > 0){
                      untouched_tasks[TaskStatus::TESTING]--;
                      updated_tasks[TaskStatus::DONE]++;
                  }
                  else{
                      break;
                  }
              }
              storage[person][TaskStatus::NEW] =
               untouched_tasks[TaskStatus::NEW] +
                updated_tasks[TaskStatus::NEW];
              storage[person][TaskStatus::IN_PROGRESS] =
               untouched_tasks[TaskStatus::IN_PROGRESS] +
                updated_tasks[TaskStatus::IN_PROGRESS];
              storage[person][TaskStatus::TESTING] =
               untouched_tasks[TaskStatus::TESTING] +
                updated_tasks[TaskStatus::TESTING];
              storage[person][TaskStatus::DONE] =
               untouched_tasks[TaskStatus::DONE] +
                updated_tasks[TaskStatus::DONE];
              // remove zero-valued keys
              clean_task(storage[person]);
              clean_task(updated_tasks);
              clean_task(untouched_tasks);
              untouched_tasks.erase(TaskStatus::DONE);
              return {updated_tasks, untouched_tasks};
          }
      };

private:
  map<string, TasksInfo> storage;
};




void PrintTasksInfo(TasksInfo tasks_info) {
  cout << tasks_info[TaskStatus::NEW] << " new tasks" <<
      ", " << tasks_info[TaskStatus::IN_PROGRESS] << " tasks in progress" <<
      ", " << tasks_info[TaskStatus::TESTING] << " tasks are being tested" <<
      ", " << tasks_info[TaskStatus::DONE] << " tasks are done" << endl;
}

void myPrint(const TasksInfo& task){
    for (const auto& [key, value]: task){
        auto tmp = static_cast<int>(key);
        cout << tmp << " " << value << endl;
    }
}



ostream& operator <<(ostream& out, const TaskStatus& status) {
    string statusName;
    switch (status) {
        case TaskStatus::NEW:
            statusName = "\"NEW\"";
            break;
        case TaskStatus::IN_PROGRESS:
            statusName = "\"IN_PROGRESS\"";
            break;
        case TaskStatus::TESTING:
            statusName = "\"TESTING\"";
            break;
        case TaskStatus::DONE:
            statusName = "\"DONE\"";
            break;
    }
    return out << statusName;
}

template <typename Collection>
string Join(const Collection& c, const string& d) {
    stringstream ss;
    bool isFirst = true;
    for (const auto& i : c) {
        if(!isFirst) {
            ss << d;
        }
        isFirst = false;
        ss << i;
    }
    return ss.str();
}
template <typename F, typename S>
ostream& operator <<(ostream& out, const pair<F,S>& p) {
    return out << p.first << ": " << p.second;
}
template <typename K, typename V>
ostream& operator <<(ostream& out, const map<K,V>& m){
    return out << '{' << Join(m, ", ") << '}';
}
template <typename T>
ostream& operator <<(ostream& out, const vector<T>& v) {
   return out << '['  << Join(v, ", ") << ']';
}

int main() {

/*
  string input_txt = "AddNewTasks Alice 5\n"
                      "PerformPersonTasks Alice 5\n"
                      "PerformPersonTasks Alice 5\n"
                      "PerformPersonTasks Alice 1\n"
                      "AddNewTasks Alice 5\n"
                      "PerformPersonTasks Alice 2\n"
                      "GetPersonTasksInfo Alice\n"
                      "PerformPersonTasks Alice 4\n"
                      "GetPersonTasksInfo Alice\n";
                      */

    string input_txt = "AddNewTasks Alice 5\n"
                       "PerformPersonTasks Alice 5\n"
                       "PerformPersonTasks Alice 5\n"
                       "PerformPersonTasks Alice 1\n"
                       "AddNewTasks Alice 5\n"
                       "PerformPersonTasks Alice 2\n"
                       "GetPersonTasksInfo Alice\n"
                       "PerformPersonTasks Alice 4\n"
                       "GetPersonTasksInfo Alice\n"
                       "PerformPersonTasks Alice 5\n"
                       "GetPersonTasksInfo Alice\n"
                       "PerformPersonTasks Alice 10\n"
                       "GetPersonTasksInfo Alice\n"
                       "PerformPersonTasks Alice 10\n"
                       "GetPersonTasksInfo Alice\n"
                       "AddNewTasks Alice 1\n"
                       "GetPersonTasksInfo Alice\n"
                       "PerformPersonTasks Alice 2\n"
                       "GetPersonTasksInfo Alice\n"
                       "PerformPersonTasks Bob 3\n";

/*
  string input_txt = "AddNewTasks Alice 1\n"
                      "PerformPersonTasks Alice 2\n"
                      "GetPersonTasksInfo Alice\n";
                      */

  TeamTasks team_tasks;
  stringstream input_stream(input_txt);
  string cmd_line;


  
  while (getline(input_stream, cmd_line)){
      stringstream cmd_stream(cmd_line);
      string cmd;
      cmd_stream >> cmd;
      if (cmd == "AddNewTasks"){
          string name;
          int num;
          cmd_stream >> name >> num;
          for (int i=0; i < num; i++){
              team_tasks.AddNewTask(name);
          }
          cout << "[]" << endl;
      }
      else if (cmd == "PerformPersonTasks"){
          string name;
          int num;
          cmd_stream >> name >> num;
          TasksInfo updated_tasks, untouched_tasks;
          tie(updated_tasks, untouched_tasks) =
           team_tasks.PerformPersonTasks(name, num);
          cout << vector<TasksInfo>{updated_tasks, untouched_tasks } << endl;
      }
      else if (cmd == "GetPersonTasksInfo"){
          string name;
          cmd_stream >> name;
          cout << team_tasks.GetPersonTasksInfo(name) << endl;
      }
      else {
          cout << cmd << endl;
//            throw invalid_argument("wrong command: " + cmd);
        }


  }



  return 0;
}
