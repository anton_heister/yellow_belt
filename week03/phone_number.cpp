#include <regex>
#include <stdexcept>
#include "phone_number.h"
#include "test_runner.h"

#include <iostream>

#include <string>

using namespace std;

const string PHONE_NUMBER_REGEX = "\\+([0-9])+\\-([0-9])+\\-(.*)+";

PhoneNumber::PhoneNumber(const string &international_number){
    bool matched = regex_match(international_number, regex(PHONE_NUMBER_REGEX));
    if (!matched){
        throw invalid_argument("Bad input: " + international_number);
    }
    size_t counter=0;
    size_t dash_counter=0;
    for (const auto& symb: international_number){
        if (counter == 0){  // skip +
            ++counter;
            continue;
        }
        else{
            if ((symb == '-') & (dash_counter < 2)){
                ++dash_counter;
                continue;
            }
            if (dash_counter == 0){
                country_code_ += symb;
            }
            else if (dash_counter == 1){
                city_code_ += symb;
            }
            else{
                local_number_ += symb;
            }
        }
    }
}


string PhoneNumber::GetCountryCode() const{
    return country_code_;
}
string PhoneNumber::GetCityCode() const{
    return city_code_;
}
string PhoneNumber::GetLocalNumber() const{
    return local_number_;
}
string PhoneNumber::GetInternationalNumber() const{
    return '+' + country_code_ + '-' + city_code_ + '-' + local_number_; 
}

/*
void TestPhoneNumber(){
    {
      string input = "+7-495-1112233";
      PhoneNumber pn(input);
      cout << "country code: "  << pn.GetCountryCode() << endl;
      AssertEqual(pn.GetCountryCode(), "7", "country code");
      AssertEqual(pn.GetCityCode(), "495", "city code");
      AssertEqual(pn.GetLocalNumber(), "1112233", "local code");
      AssertEqual(pn.GetInternationalNumber(), input, "intenational number");
    }
    {
     string input = "+323-22-460002";
      PhoneNumber pn(input);
    }
    {
     string input = "+1-2-coursera-cpp";
      PhoneNumber pn(input);
    }
    {
     string input = "1-2-333";
     try{
      PhoneNumber pn(input);
      }
      catch (invalid_argument){
          AssertEqual(1, 1);
      }
    }
    {
     string input = "+7-1233";
     try{
      PhoneNumber pn(input);
      }
      catch (invalid_argument){
          AssertEqual(1, 1);
      }
    }

}



int main(){
    TestRunner tr;
    tr.RunTest(TestPhoneNumber, "TestPhoneNumber");
    return 0;
}
*/