#include <string>
//#include <iostream>
#include <vector>
//#include <map>

#include "bus_manager.h"
//#include "responses.h"

using namespace std;


void BusManager::AddBus(const string& bus, const vector<string>& stops) {
    this->buses_to_stops.insert(make_pair(bus, stops));
    for (const auto& stop : stops) {
      this->stops_to_buses[stop].push_back(bus);
    }
  }

BusesForStopResponse BusManager::GetBusesForStop(const string& stop) const {
    if (this->stops_to_buses.count(stop) == 0) {
      return BusesForStopResponse{vector<string>()};
    } else {
      return BusesForStopResponse{this->stops_to_buses.at(stop)};
    }
  }

StopsForBusResponse BusManager::GetStopsForBus(const string& bus) const {
    vector<pair<string, vector<string>>> result;
    if (this->buses_to_stops.count(bus) > 0) {
      for (const auto& stop : this->buses_to_stops.at(bus)) {
        result.push_back(make_pair(stop, this->stops_to_buses.at(stop)));
      }
    }
    return StopsForBusResponse{bus, result};
  }

AllBusesResponse BusManager::GetAllBuses() const {
    return AllBusesResponse{this->buses_to_stops};
  }