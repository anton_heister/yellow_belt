#pragma once
#include <iostream>

using namespace std;

enum class Comparison{
    Less=0, LessOrEqual, Greater, GreaterOrEqual, Equal, NotEqual};

ostream& operator<<(ostream& out, const Comparison value);

enum class LogicalOperation{Or, And};

string ParseEvent(istream& is);