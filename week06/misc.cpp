#include "misc.h"
#include <iostream>
#include <string>

using namespace std;

ostream& operator<<(ostream& out, const Comparison value){
    const char* s = 0;
#define PROCESS_VAL(p) case(p): s = #p; break;
    switch(value){
        PROCESS_VAL(Comparison::Less);     
        PROCESS_VAL(Comparison::LessOrEqual);     
        PROCESS_VAL(Comparison::Greater);
        PROCESS_VAL(Comparison::GreaterOrEqual);
        PROCESS_VAL(Comparison::Equal);
        PROCESS_VAL(Comparison::NotEqual);
    }
#undef PROCESS_VAL
    return out << s;
}

const string WHITESPACE = " \n\r\t\f\v";

string ltrim(const string& s)
{
    size_t start = s.find_first_not_of(WHITESPACE);
    return (start == std::string::npos) ? "" : s.substr(start);
}

string ParseEvent(istream& is) {
  string event;
  getline(is, event);
  return ltrim(event);
}