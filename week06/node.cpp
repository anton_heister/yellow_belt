#include "node.h"
#include <memory>
#include "date.h"
#include "misc.h"
#include <iostream>

using namespace std;


Node::Node(const string& type) :_type(type){};
string Node::get_type() const{
    return _type;
}


EmptyNode::EmptyNode()
 :Node("Empty"){};
bool EmptyNode::Evaluate(const Date& date, const string& event) const {
    return true;}


DateComparisonNode::DateComparisonNode(const Comparison cmp, const Date date)
    :Node("DateComparison"), _cmp(cmp), _date(date){
//        cout << "Operation " << _cmp << endl;
    };

bool DateComparisonNode::Evaluate(const Date& date, const string& event) const {
        if (_cmp == Comparison::Equal){
            return date == _date;
        }
        else if (_cmp == Comparison::NotEqual){
            return date != _date;
        }
        else if (_cmp == Comparison::Less){
            return date < _date;
        }
        else if (_cmp == Comparison::LessOrEqual){
            return date <= _date;
        }
        else if (_cmp == Comparison::Greater){
            return date > _date;
        }
        else if (_cmp == Comparison::GreaterOrEqual){
            return date >= _date;
        }
        else{
            throw logic_error("Unsupported comparison operator.");
        }
    };

Comparison DateComparisonNode::get_op() const{
    return _cmp;
};


EventComparisonNode::EventComparisonNode(const Comparison cmp, const string value)
    :Node("EventComparison"), _cmp(cmp), _value(value){};

bool EventComparisonNode::Evaluate(const Date& date, const string& event) const {
        if (_cmp == Comparison::Equal){
            return event == _value;
        }
        else if (_cmp == Comparison::NotEqual){
            return event != _value;
        }
        else if (_cmp == Comparison::Less){
            return event < _value;
        }
        else if (_cmp == Comparison::Greater){
            return event > _value;
        }
        else if (_cmp == Comparison::LessOrEqual){
            return event <= _value;
        }
        else if (_cmp == Comparison::GreaterOrEqual){
            return event >= _value;
        }
       else{
           throw logic_error("Unsupported comparison operator.");
       }
    };


LogicalOperationNode::LogicalOperationNode(const LogicalOperation op,
 shared_ptr<const Node> left, shared_ptr<const Node> right)
 :Node("LogicalOperation"), _op(op), _left(left), _right(right){};

bool LogicalOperationNode::Evaluate(const Date& date, const string& event) const{
    if (_op == LogicalOperation::And){
        return _left->Evaluate(date, event) && _right->Evaluate(date, event);
    }
    else if (_op == LogicalOperation::Or){
        return _left->Evaluate(date, event) || _right->Evaluate(date, event);
    }
    else{
        throw logic_error("Unsupported logical operator.");
    }
} ;