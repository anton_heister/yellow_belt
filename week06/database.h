#pragma once
#include <algorithm>
#include <deque>
#include <iostream>
#include <map>
#include <memory>
#include <set>
#include <utility>
#include <vector>

#include "date.h"
#include "node.h"

using namespace std;

typedef pair<Date, string> Entry;

bool operator!=(const Entry& lhs, const Entry& rhs) {
    if ((lhs.first != rhs.first) || (lhs.second != rhs.second)) {
        return true;
    } else {
        return false;
    }
}

ostream& operator<<(ostream& stream, const Entry& entry);

typedef map<Date, set<string>> SetStorage;
typedef map<Date, deque<string>> DequeStorage;

class Database {
   public:
    void Add(const Date& date, const string& event);

    void Print(ostream& cout) const;

    template <class Func>
    int RemoveIf(Func func);

    template <class Func>
    vector<Entry> FindIf(Func func) const;

    Entry Last(const Date& date) const;

   private:
    SetStorage storage;
    DequeStorage ordered_storage;
};

void Database::Add(const Date& date, const string& event) {
    auto tmp = storage[date].insert(event);
    if (tmp.second){ // insertion took place
        ordered_storage[date].push_back(event);
    }
}

/*
template <class Func>
int Database::RemoveIf(Func predicate) {
    // clean ordered storage
    int counter = 0;
    auto it = ordered_storage.begin();
    while (it != ordered_storage.end()) {
        auto date = it->first;
        auto erase_it = stable_partition(it->second.begin(), it->second.end(),
                                         [date, predicate](const string& event) { return !predicate(date, event); });
        auto n_to_erase = it->second.end() - erase_it;
        counter += n_to_erase;
        bool erase_all = n_to_erase == storage.at(date).size();
        if (erase_all) {
            it = ordered_storage.erase(it);
            storage.erase(date);
        } else {
            auto erase_it_copy = erase_it;
            for (; erase_it_copy != it->second.end(); ++erase_it_copy) {
                storage.at(date).erase(*erase_it);
            }
            it->second.erase(erase_it, it->second.end());
            ++it;
        }
    }
    return counter;
}
*/


template <class Func>
int Database::RemoveIf(Func predicate) {
    // clean ordered storage
    int counter = 0;
    auto it = ordered_storage.begin();
    while (it != ordered_storage.end()) {
        auto date = it->first;
        auto erase_it = stable_partition(it->second.begin(), it->second.end(),
                                         [date, predicate](const string& event) { return !predicate(date, event); });
        auto n_to_erase = it->second.end() - erase_it;
        counter += n_to_erase;
        bool erase_all = n_to_erase == storage.at(date).size();
        if (erase_all) {
            it = ordered_storage.erase(it);
            storage.erase(date);
        } else {
            auto erase_it_copy = erase_it;
            for (; erase_it_copy != it->second.end(); ++erase_it_copy) {
                storage.at(date).erase(*erase_it);
            }
            it->second.erase(erase_it, it->second.end());
            ++it;
        }
    }
    return counter;
}


template <class Func>
vector<Entry> Database::FindIf(Func predicate) const {
    vector<Entry> result;
    for (auto it = ordered_storage.begin(); it != ordered_storage.end(); ++it) {
        auto date = it->first;
        for (auto event_it = it->second.begin(); event_it != it->second.end(); ++event_it) {
            if (predicate(date, *event_it)) {
                result.push_back(make_pair(date, *event_it));
            }
        }
    }
    return result;
}

Entry Database::Last(const Date& date) const {
    auto date_it = storage.upper_bound(date);
    if (date_it == storage.begin()) {
        throw invalid_argument("Date earlier than entries.");
    }
    date_it = prev(date_it);
    auto found_date = date_it->first;
    auto found_event = *(ordered_storage.at(found_date).rbegin());
    return make_pair(found_date, found_event);
}

void Database::Print(ostream& stream) const {
    for (const auto& item : ordered_storage) {
        //for (const auto& item : storage) {
        for (const string& event : item.second) {
            stream << item.first << " " << event << endl;
        }
    }
}

ostream& operator<<(ostream& stream, const Entry& entry) {
    stream << entry.first << " " << entry.second;
    return stream;
};