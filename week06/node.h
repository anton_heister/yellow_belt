#pragma once
#include "date.h"
#include "misc.h"
#include <memory>

using namespace std;

class Node{
public:
    Node(const string& type);
    virtual bool Evaluate(const Date& date, const string& event) const = 0;
    const string _type;
    string get_type() const;
};

class EmptyNode: public Node{
public:
    EmptyNode();
    bool Evaluate(const Date& date, const string& event) const override;
};

class DateComparisonNode: public Node{
public:
    DateComparisonNode(const Comparison cmp, const Date date);
    bool Evaluate(const Date& date, const string& event) const override;
    Comparison get_op() const;
private:
    const Comparison _cmp;
    const Date _date;
};

class EventComparisonNode: public Node{
public:
    EventComparisonNode(const Comparison cmp, const string value);
    bool Evaluate(const Date& date, const string& event) const override;
private:
    const Comparison _cmp;
    const string _value;

};

class LogicalOperationNode: public Node{
public:
    LogicalOperationNode(const LogicalOperation op, shared_ptr<const Node> left,
    shared_ptr<const Node> right);
    bool Evaluate(const Date& date, const string& event) const override;
private:
    const LogicalOperation _op; 
    shared_ptr<const Node> _left, _right;
};