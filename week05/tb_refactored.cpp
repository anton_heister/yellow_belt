#include <iostream>
#include <string>
#include <vector>

using namespace std;

class Person {
public:
	Person(const string& p_name, const string& p_occupation = "") {
		name = p_name;
		occupation = p_occupation;
	}
	virtual ~Person() {};

	virtual void Walk(const string& destination) const {
		string valid_occupation = occupation.empty() ? "" : occupation + ": ";
		cout << valid_occupation << name << " walks to: " << destination << endl;
	}

public:
	string name;
	string occupation;
};

class Student : public Person {
public:
    Student(const string& s_name, const string& s_favourite_song)
		: Person(s_name, "Student") {
        favourite_song = s_favourite_song;
    }

    void Learn() const {
        cout << occupation << ": " << name << " learns" << endl;
    }

    virtual void Walk(const string& destination) const override {
        Person::Walk(destination);
        SingSong();
    }

    void SingSong() const {
        cout << occupation << ": " << name << " sings a song: " << favourite_song << endl;
    }

public:
    string favourite_song;
};


class Teacher : public Person {
public:

    Teacher(const string& t_name, const string& t_subject)
		: Person(t_name, "Teacher") {
        subject = t_subject;
    }

    void Teach() const {
        cout << occupation << ": " << name << " teaches: " << subject << endl;
    }

public:
    string subject;
};


class Policeman : public Person {
public:
    Policeman(const string& p_name)
		: Person(p_name, "Policeman") {
    }

    void Check(const Person& p) {
    	cout << occupation << ": " << name << " checks " << p.occupation << ". " <<
    			p.occupation << "'s name is: " << p.name << endl;
    }
};

void VisitPlaces(const Person& person, vector<string> places) {
    for (auto place : places) {
        person.Walk(place);
    }
}

int main() {
    Teacher t("Jim", "Math");
    Student s("Ann", "We will rock you");
    Policeman p("Bob");

    VisitPlaces(t, {"Moscow", "London"});
    p.Check(s);
    VisitPlaces(s, {"Moscow", "London"});
    return 0;
}
