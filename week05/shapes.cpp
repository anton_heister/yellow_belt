#include <iostream>
#include <vector>
#include <memory>
#include <math.h>
#include <sstream>
#include <iomanip>
#include <exception>

using namespace std;

const float PI = 3.14;


class Figure{
    public:
      Figure(const string& name_)
      :name(name_){};
      //virtual string Name() const = 0;  
      virtual string Name()const {
          return name;
      };
      virtual float Perimeter() const = 0;  
      virtual float Area() const = 0;  
      const string name;
};

class Rect: public Figure{
    public:
    Rect(unsigned int height_, unsigned int width_):
     Figure("RECT"){
         height = height_;
         width = width_;
    }
//    string Name() const override{
//        return name;
//    }
    float Perimeter() const override{
        return 2*height + 2*width;
    }
    float Area() const override{
        return height * width;
    }

    private:
    unsigned int height, width;
};

class Triangle: public Figure{
    public:
    Triangle(unsigned int a_, unsigned int b_, unsigned int c_)
    : Figure("TRIANGLE"){
        a = a_;
        b = b_;
        c = c_;
    };
    float Perimeter() const override{
        return a + b + c;
    }
    float Area() const override{
        float p2 = static_cast<float>(Perimeter() / 2);
        float area = static_cast<float>(p2 * (p2 - a) * (p2 - b) * (p2 - c));
        return static_cast<float>(sqrt(area));
    }
    private:
    float a, b, c;
    const string name;
};

class Circle: public Figure{
    public: 
    Circle(unsigned int r_)
    :Figure("CIRCLE"){
        r = r_;
    }
    float Perimeter() const override{
        return static_cast<float>(2 * PI * r);
    }
    float Area() const override{
        return static_cast<float>(PI * r * r);

    }
    private:
    unsigned int r;
};



//template <class T>
//shared_ptr<T> CreateFigure(stringstream& is){
shared_ptr<Figure> CreateFigure(istream& is){
    string shape;
    is >> shape;
    if (shape == "RECT"){
        unsigned int width, height;
        is >> width;
        is >> height;
        return make_shared<Rect>(width, height);
    }
    else if (shape == "TRIANGLE"){
        unsigned int a, b, c;
        is >> a >> b >> c;
        Triangle figure({a, b, c});
        return make_shared<Triangle>(a, b, c);
    }
    else if (shape == "CIRCLE"){
        unsigned int r;
        is >> r;
        return make_shared<Circle>(r);
    }
    else{
        throw "Shape not supported.";
    }

      //ADD RECT width height —  добавить прямоугольник с размерами width и height (например, ADD RECT 2 3).
      //ADD TRIANGLE a b c —  добавить треугольник со сторонами a, b и c (например, ADD TRIANGLE 3 4 5).
      //ADD CIRCLE r —  добавить круг радиуса r (например, ADD CIRCLE 5).
}   

int main() {
  vector<shared_ptr<Figure>> figures;
  for (string line; getline(cin, line); ) {
    istringstream is(line);

    string command;
    is >> command;
    if (command == "ADD") {
        string shape;
      // Пропускаем "лишние" ведущие пробелы.
      // Подробнее об std::ws можно узнать здесь:
      // https://en.cppreference.com/w/cpp/io/manip/ws

      is >> ws;
      figures.push_back(CreateFigure(is));
    } else if (command == "PRINT") {
      for (const auto& current_figure : figures) {
        cout << fixed << setprecision(3)
             << current_figure->Name() << " "
             << current_figure->Perimeter() << " "
             << current_figure->Area() << endl;
      }
    }
  }
  return 0;
}