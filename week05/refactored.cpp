
#include <iostream>
#include <string>
#include <vector>

using namespace std;


class Person {
    public:
    Person(const string& name_, const string& role_):
     Name(name_), Role(role_)
    {};
    const string Name;
    const string Role;  // student, policeman, teacher

    virtual void Walk(const string& destination) const {
        cout << Role << ": " << Name << " walks to: " << destination << endl;
    }
};


class Student : public Person{
public:

    Student(string name, string favouriteSong):
    Person(name, "Student") {
        FavouriteSong = favouriteSong;
    }

    void Learn() const {
        cout << Role << ": " << Name << " learns" << endl;
    }

    void Walk(const string& destination) const override {
        cout << Role << ": " << Name << " walks to: " << destination << endl;
        SingSong();
    }

    void SingSong() const {
        cout << Role << ": " << Name << " sings a song: " << FavouriteSong << endl;
    }

public:
    string FavouriteSong;
};


class Teacher : public Person {
public:

    Teacher(string name, string subject)
    :Person(name, "Teacher") {
        Subject = subject;
    }

    void Teach() const {
        cout << Role << ": " << Name << " teaches: " << Subject << endl;
    }

public:
    string Subject;
};


class Policeman : public Person {
public:
    Policeman(string name)
    :Person(name, "Policeman") {
    }

    void Check(const Person& t) const {
        cout << Role << ": " << Name << " checks " << t.Role << ". " << t.Role
         << "'s name is: " << t.Name << endl;
    }
};

void VisitPlaces(const Person& t, const vector<string>& places) {
    for (auto p : places) {
        t.Walk(p);
    }
}


int main() {
    Teacher t("Jim", "Math");
    Student s("Ann", "We will rock you");
    Policeman p("Bob");

    VisitPlaces(t, {"Moscow", "London"});
    p.Check(s);
    VisitPlaces(s, {"Moscow", "London"});
    return 0;
}
